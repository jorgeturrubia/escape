from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User

# Create your models here.

class Juego(models.Model):
    title = models.CharField(max_length=200, verbose_name="Título")
    content = models.TextField(verbose_name="Contenido")
    published = models.DateTimeField(verbose_name="Fecha de publicación", default=now)
    image = models.ImageField(verbose_name="Imagen", upload_to="blog", null=True, blank=True)
    author = models.ForeignKey(User, verbose_name="Autor", on_delete=models.CASCADE)  
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")    

    class Meta:
        verbose_name = "juego"
        verbose_name_plural = "juegos"
        ordering = ['-created']

    def __str__(self):
        return self.title

class Nivel(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nivel")
    tituloDelNivel=models.CharField(max_length=100, verbose_name="Titulo del Nivel", null=True)
    subtituloDelNivel= models.CharField(max_length=100, verbose_name="Subtitulo del nivel",null=True)
    numeroNivel=models.IntegerField(verbose_name="Número de nivel", null=True)
    image = models.ImageField(verbose_name="Imagen Nivel", upload_to="nivel", null=True, blank=True)
    clave=models.CharField(max_length=20, verbose_name="Clave", null=True)
    historiaNivel=models.TextField(verbose_name="Contenido del Nivel", null=True)
    juego = models.ForeignKey(Juego, verbose_name="Juego asociado", on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "nivel"
        verbose_name_plural = "niveles"
        ordering = ['-created']

    def __str__(self):
        return self.name

