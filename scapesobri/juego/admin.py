  
from django.contrib import admin
from .models import Nivel, Juego

# Register your models here.
class NivelAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

class JuegoAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('title', 'author', 'published')
    ordering = ('author', 'published')
    search_fields = ('title','content','author__username')
    date_hierarchy = 'published'
    list_filter = ('author__username',)



admin.site.register(Nivel, NivelAdmin)
admin.site.register(Juego, JuegoAdmin)