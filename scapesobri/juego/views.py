from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect
from .models import Juego,Nivel
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.urls import reverse, reverse_lazy
from django.forms import ModelForm
from .forms import NivelForm
from django.db.models import FilteredRelation,Q



# Create your views here.

class JuegoListView(ListView):  
   model = Juego

class JuegoDetailView(DetailView):
   
   model = Juego
   success_url = reverse_lazy('juegos:juegos')  
  

class JuegoCreate(CreateView):
   model = Juego
  
   success_url = reverse_lazy('juegos:juegos')  



class NivelUpdate(UpdateView):
  model = Nivel
  form_class = NivelForm
  template_name_suffix = '_update_form'

   
class NivelListView(ListView):
  model = Nivel

class NivelDetailView(DetailView):
   
  model = Nivel


def comprobarclave(request,nivel_id):
   nivel = get_object_or_404(Nivel, pk=nivel_id)
 
   resultadoClave = request.POST['clave']

   consultaClave = nivel.clave

   objetosNivel = Nivel.objects.all()
   countNivel=len(objetosNivel)

   if(nivel_id<countNivel and resultadoClave==consultaClave):   
      return HttpResponseRedirect(reverse('juegos:nivel', args=
      (nivel_id+1,)))
   if(resultadoClave!=consultaClave):
       return HttpResponseRedirect(reverse('juegos:nivel', args=
      (nivel_id,)))
   if(nivel_id==countNivel):
      # return HttpResponseRedirect(reverse('juegos:nivelfinal'))
      return render(request, 'juego/final.html')

def nivelesJuegoSeleccionado(request,juego_id):
   idjuego=juego_id
  
   ObjetoNivel=Nivel.objects.get(juego_id=idjuego,numeroNivel=1)
   nivel_id = ObjetoNivel.pk

   print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
   print(idjuego)
   print(nivel_id)
 
 
   return HttpResponseRedirect(reverse('juegos:nivel', args=
      (nivel_id,)))



  


 



