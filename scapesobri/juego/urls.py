from django.urls import path
from .views import JuegoListView,JuegoDetailView,NivelDetailView,NivelListView,JuegoCreate,NivelUpdate,comprobarclave,nivelesJuegoSeleccionado
from . import views


juego_patterns = ([
    #Paht Index
    path('',JuegoListView.as_view(), name="juegos"),
    path('<int:pk>/<slug:slug>/',JuegoDetailView.as_view(),name="juego"),
    path('niveles/',NivelListView.as_view(),name="niveles"),
    path('nivel/<int:pk>/',NivelDetailView.as_view(),name="nivel"),
    path('create/',JuegoCreate.as_view(), name="create"),
    path('update/nivel/<int:pk>/',NivelUpdate.as_view(), name="update"),
    path('<int:nivel_id>/comprobarclave', views.comprobarclave, name ="comprobarclave"),
     path('<int:juego_id>/nivelesJuegoSeleccionado', views.nivelesJuegoSeleccionado, name ="nivelesJuegoSeleccionado"),
   
    
],'juegos')