
from django import forms
from .models import Nivel

class NivelForm(forms.ModelForm):  
    

        class Meta:

            model = Nivel
            fields = ['clave',]
            widgets ={
                'clave':forms.TextInput
            }
       
     